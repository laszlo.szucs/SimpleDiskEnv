# 
# Code to set up a simple RADMC-3D disk models with envelope
#
# The code heavily relies on radmc3dPy package by Attila Juhasz:
# (https://www.ast.cam.ac.uk/~juhasz/radmc3dPyDoc/index.html)
#
# Original SimpleDisk source available at:
# https://gitlab.mpcdf.mpg.de/szucs/SimpleDisk
#  
# Copyright (C) 2018 Laszlo Szucs <laszlo.szucs@mpe.mpg.de>
#
# Licensed under GLPv2, for more information see the LICENSE file repository.
#

from __future__ import absolute_import
from __future__ import print_function

# import SimpleDisk modules
from SimpleDiskEnv import ulrich_envelope

# import 3rd party modules
import os
import numpy as np
import matplotlib.pyplot as plt

import radmc3dPy
from radmc3dPy import natconst
from radmc3dPy import analyze

def ISradField(G=None,grid=None,ppar=None,show=False,write=False):
   '''
   Computes the spectral energy distribution of the Interstellar Radiation 
   Field, based on Draine (1978) and Black (1994) prescription.
   If requested then written to RADMC-3D external_source.inp file and 
   displayed on screen.

   Note that G only scales the FUV component, described by Draine and not 
   the both components.
   
   '''
   
   cc = natconst.cc   # speed of light in cgs
   kk = natconst.kk   # Boltzmann constant in cgs
   hh = natconst.hh   # Planck constant in cgs

   erg2eV = 6.242e11  # conversion from electronvolt to erg
      
   # Determine the G scale parameter of the FUV field
   G0 = 1.7
   if (G==None) and ('G' in  ppar.keys()):
      G = ppar['G']
   elif (G==None):
      G = G0
   
   # Get the wavelength and frequency ranges of the model from grip object
   wav = grid.wav
   nwav = grid.nwav
   freq = grid.freq
   nfreq = grid.nfreq
   eV = hh * freq * erg2eV   # erg -> eV


   # Create the black body components of the Black (1994) radiation field:
   # see: https://ui.adsabs.harvard.edu/#abs/1994ASPC...58..355B/abstract
   p = [0., 0., 0., 0., -1.65, 0.]
   T = [7500.,4000.,3000.,250.,23.3,2.728]
   W = [1e-14, 1e-13, 4e-13, 3.4e-09, 2e-4, 1.]
   lp = [0.4,0.75,1.,1.,140.,1060.]
   
   IBlack = np.zeros(nwav)

   for i in range(len(p)):
       exppart = np.array(hh * freq / kk / T[i])
       exppart= np.clip(exppart,0,500)
       IBlack = IBlack + ( 2.*hh*freq**3. / cc**2. * (wav / lp[i])**p[i]    \
              * W[i] / (np.exp(exppart, dtype=np.float64)-1))
   # don't worry about the warnings about division by zero, 
   # it comes from the (np.exp(hh * freq / kk / T[i])-1) part.
   
   # Create the Draine (1978) radiation field using his original formula, 
   # see eq. 11 in https://ui.adsabs.harvard.edu/#abs/1978ApJS...36..595D/abstract
   # in photons cm^-2 s^-1 sr^-1 eV^-1
   IDraineEv = (1.658E6 * eV) - (2.152e5 * eV**2) + (6.919E3 * eV**3)
   # in erg cm^-2 s^-1 sr^-1 Hz^-1
   IDraineErg = IDraineEv * hh**2 * freq * erg2eV                      
   
   # scale the FUV Draine field
   IDraine = IDraineErg * G/G0
   # The formula was designed on the 5 eV (0.24 micron) to 13.6 eV (0.09 micron) range,
   # limit the calculated intensities
   IDraine[wav < 0.09117381] = 0.0
   IDraine[wav > 0.24799276] = 0.0
   IBlack[wav < 0.24799276] = 0.0           # limit the Black field as well below 0.24 micron

   # Combine the expressions for the different wavelength ranges:
   Iisrf = IBlack + IDraine
   #
   # Plot the results if asked
   #
   # Measurements to overplot from Allen book ISRF, unit: erg s-1 cm-2 mum-1 
   if show:
      wavObs = np.array([0.091, 0.1, 0.11, 0.13, 0.143, 0.18,  \
                         0.2, 0.21, 0.216, 0.23, 0.25, 0.346,  \
                         0.435, 0.55, 0.7, 0.9, 1.2, 1.8, 2.2, \
                         2.4, 3.4, 4, 5, 12, 25, 60, 100, 200, \
                         300, 400, 600, 1000])
      freqObs_hz = cc / (wavObs / 1e4)
      FlamObs  = np.array([1.07e-2, 1.47e-2, 2.04e-2, 2.05e-2, 1.82e-2, \
                           1.24e-2, 1.04e-2, 9.61e-3, 9.17e-3, 8.25e-3, \
                           7.27e-3, 1.3e-2, 1.5e-2, 1.57e-2, 1.53e-2,   \
                           1.32e-2, 9.26e-3, 4.06e-3, 2.41e-3, 1.89e-3, \
                           6.49e-4, 3.79e-4, 1.76e-4, 1.7e-4, 6.0e-5,   \
                           4.6e-5, 7.3e-5, 2.6e-5, 5.4e-6, 1.72e-6,     \
                           3.22e-6, 7.89e-6])
      InuObs = (wavObs / 1e4)**2. / cc * FlamObs / (4.*3.14) * 1e4
      # Plot   
      plt.xscale('log')
      plt.yscale('log')
      plt.plot(wavObs, InuObs * freqObs_hz, 'ro')
      plt.plot(wav, Iisrf * freq, color='black')
      plt.xlabel('wavelength [$\mu$m]')
      plt.ylabel('intensity [erg s$^{-1}$ cm$^{-2}$ Hz$^{-1}$ sr$^{-1}$]')
      plt.show()
   #
   # Write to file is asked
   #
   if write:   
      fname = 'external_source.inp'
      print('Writing {}'.format(fname))
      wfile = open(fname, 'w')
      wfile.write('%d\n'%2)     # this is the format, should be 2 always
      wfile.write('%d\n'%nwav)  # number of wavelength
      for ilam in range(nwav):  # first wavelength then Inu ISRF
          wfile.write('%.9e\n'%wav[ilam])
      for ilam in range(nwav):
          wfile.write('%.9e\n'%Iisrf[ilam])
      wfile.close()
            
def simpleRadmc3Dmodel(disk=True, envelope=True, cavity=False, isrf=False,
                       shisrf=False, G=1.7, paramfile=None):
   '''
   Creates RADMC-3D disk + envelope model input files.
   '''

   au = natconst.au   # astronomical unit in cgs
   pc = natconst.pc   # parsec in cgs
   ms = natconst.ms   # Solar mass in cgs
   rs = natconst.rs   # Solar radius in cgs

   # Read the parameters from the problem_params.inp file 
   modpar = readParams(paramfile=paramfile)

   # Make a local copy of the ppar dictionary
   ppar = modpar.ppar

   # Print the important used parameters:

   # Stellar properties:
   print("\nStellar properties:\n")
   print(" T_star = {:.2f} K".format(ppar['tstar'][0]))
   print(" R_star = {:.2f} Rsol = {:.2f} AU".format(ppar['rstar'][0]/rs,   \
                                                  ppar['rstar'][0]/au))
   Lstar = (ppar['rstar'][0]/rs)**2 * (ppar['tstar'][0]/5772.)**4
   print(" L_star = {:.2E} Lsol".format(Lstar))
   # Grid parameters
   print("\nGrid parameters:\n")
   print(" coordinate system: {}".format(ppar['crd_sys']))
   print(" x coordinate boundaries: {} in AU".format(
       np.array(ppar['xbound']) / au))
   print(" nx = {}".format(ppar['nx']))
   print(" y coordinate boundaries: {} in rad".format(ppar['ybound']))
   print(" ny = {}".format(ppar['ny']))
   if ('zbound' in ppar.keys()):
      print(" z coordinate boundaries: {}".format(ppar['zbound']))
      print(" nz = {}".format(ppar['nz']))
   else:
      print(" z coordinate is not activated (2D model)!")
   if ('xres_nlev' in ppar.keys()):
      print(" Refinement along x axis:")
      print(" in {} steps".format(ppar['xres_nstep']))
   if (ppar["ngs"] == 2) and disk and envelope:
      print(" Different opacity in disk and envelope!")
   
   # Wavelength grid
   print("\nWavelength grid:\n")
   print(" Wavelength ranges {} in micron".format(ppar['wbound']))
   print(" Bin number in range {}".format(ppar['nw']))

# --------------------------------------------------------------------------------------------
# Create the grid
# --------------------------------------------------------------------------------------------

   # create the radmc3dGrid object:
   grid = analyze.radmc3dGrid()
   # create the wavelength grid
   grid.makeWavelengthGrid(ppar=ppar)
   # create the spatial grid
   grid.makeSpatialGrid(ppar=ppar)

# --------------------------------------------------------------------------------------------
# Create the input stellar radiation field
# --------------------------------------------------------------------------------------------

   radSources = analyze.radmc3dRadSources(ppar=ppar, grid=grid)
   radSources.getStarSpectrum(tstar=ppar['tstar'], rstar=ppar['rstar'])

# --------------------------------------------------------------------------------------------
# Create the dust density distribution 
# --------------------------------------------------------------------------------------------

   # Create a radmc3dData object, this will contain the density
   data = analyze.radmc3dData(grid)

   # Creat a grid for the 
   rr, th = np.meshgrid(grid.x, grid.y, indexing='ij')
   z0 = np.zeros([grid.nx, grid.nz, grid.nz], dtype=np.float64)
   zz   = rr * np.cos(th)
   rcyl = rr * np.sin(th)

   # Recenter the grid; used for modeCav = 'gridcenter'
   zz_n = 0.0
   rcyl_n = ppar['rTrunEnv']
   rr_n = np.sqrt( (rcyl-rcyl_n)**2 + (zz-zz_n)**2 )
   th_n = np.arctan( (rcyl-rcyl_n) / (zz-zz_n) )
    
#======================== Backgroud density =========================#

   rho_bg = np.zeros([grid.nx, grid.ny, grid.nz, 1], dtype=np.float64) \
          + (ppar['bgdens'] * ppar['dusttogas'])

#============ Envelope density + Cavity + Cut off radius ============#

   # Envelope parameters:
   print("\nEnvelope parameters:\n")
   if envelope == True:
       
      # Ensure backward compatibility
      if not 'modeEnv' in ppar:
         ppar['modeEnv'] = 'powerlaw'
      if not 'redFactCav' in ppar:
         ppar['redFactCav'] = 0.1
      if not 'redFactEnv' in ppar:
         ppar['redFactEnv'] = 0.0
         
      print(" {:s} envelope is included!".format(ppar['modeEnv']))
      print(" Density at {:.2f} AU = {:.2E} g/cm^3".format(ppar['r0Env'] / au, \
                                                          ppar['rho0Env']))
      if  (ppar['modeEnv'] != 'Ulrich1976'):
         print(" Density power law index = {:.2f}".format(ppar['prhoEnv']))
      print(" Truncation radius = {:.2f} AU".format(ppar['rTrunEnv'] / au))
      print(" Density reduction fact. within tranc. radius = {:.2E}".format(   \
                                                         ppar['redFactEnv']))
      if cavity:
         print("\n Model contains cavity with parameters:")
         print(" Cavity mode: {:s}".format(ppar['modeCav']))
         print(" Opening angle = {:.2f} deg".format(ppar['thetDegCav']))
         print(" Density reduction factor = {:.2E}".format(ppar['redFactCav']))
   else:
      print(" *NO* envelope is included!")

   rho_env = np.zeros([grid.nx, grid.ny, grid.nz, 1], dtype=np.float64) 

   if envelope == True:
      if (ppar['modeEnv'] == 'Ulrich1976'):
          dummy = ulrich_envelope.ulrich_envelope(rr,th,  \
                  rho0=ppar['rho0Env']/ppar['dusttogas'], \
                  rmin=ppar['rTrunEnv'], Rc=ppar['r0Env'])
      elif (ppar['modeEnv'] == 'Tafalla2004'):
          dummy = ppar['rho0Env'] * 1./(1. + (rr/ppar['r0Env'])**(-1.0*ppar['prhoEnv']))
          crit = ( rr < ppar['rTrunEnv'] ) 
          dummy[crit] = dummy[crit] * ppar['redFactEnv']
      elif (ppar['modeEnv'] == 'powerlaw'):
          dummy = ppar['rho0Env'] * (rr/ppar['r0Env'])**ppar['prhoEnv']
          crit = ( rr < ppar['rTrunEnv'] ) 
          dummy[crit] = dummy[crit] * ppar['redFactEnv']
      else:
          raise ValueError('Unknown envelope mode: modeEnv = {:s}'.format(ppar['modeEnv']))
      rho_env[:,:,0,0] = dummy
                     
      if cavity == True:
         dummy = rho_env[:,:,0,0]
         if (ppar['modeCav'] == 'Sheehan2017'):
            crit = ( zz > (1.0*au + rcyl**(1.0)) )
         elif (ppar['modeCav'] == 'edgecenter'):
            crit = ( th_n < np.deg2rad(ppar['thetDegCav']) )
         elif (ppar['modeCav'] == 'gridcenter'):
            crit = ( th < np.deg2rad(ppar['thetDegCav']) )
         else:
            raise ValueError('Unknown cavity mode: modeCav = {:s}'.format(ppar['modeEnv']))
         dummy[crit] = dummy[crit] * ppar['redFactCav']
         rho_env[:,:,0,0] = dummy

      # Calculate the volume of each grid cell
      vol  = grid.getCellVolume()
      mass = (2.0*rho_env[:,:,:,0]*vol).sum(0).sum(0).sum(0)
      mass3000 = ( (2.0*rho_env[:,:,:,0]*vol)[(grid.x<=3000.*au),:,:] ).sum(0).sum(0).sum(0)
      print("\n Menv (<3000 au, gas+dust) = {:.2E} Msol".format(mass3000/ms))
      print(" Menv (final, gas+dust) = {:.2E} Msol".format(mass/ms))

   # Convert the gas+dust mass to dust mass
   rho_env = rho_env * ppar['dusttogas']   

#========================= Flarring disk ============================#

   # set up the arrays containing the disk density
   # gas + dust
   rho_disk_tot  = np.zeros([grid.nx, grid.ny, grid.nz,1], dtype=np.float64)
   # only dust
   rho_disk_dust = np.zeros([grid.nx, grid.ny, grid.nz,1], dtype=np.float64)

# Disk parameters:
   print("\nDisk parameters:\n")
   if disk == True:
      print(" Disk is included in the model!")
      if ('mdisk' in ppar.keys()): 
          print("Mdisk (dust+gas) = {:.2E} Msol".format(ppar['mdisk'] / ms))
      else:
          print(" Mdisk is not set, using Sig(0)")
          if ('sig0' in ppar.keys()): 
              print(" Sig0(Rsig) = {:.2E} g/cm^2".format(ppar['sig0']))
          else:
              raise ValueError("Keyword sig0 is not set!")
          if ('rsig' in ppar.keys()): 
              print(" Rsig = {:.2f} AU".format(ppar['rsig'] / au))
          else: 
              print(" Rsig = {:.2f} AU".format(ppar['rdisk'] / au))
      print(" Rin = {:.2f} AU".format(ppar['rin'] / au))
      print(" Rdisk = {:.2f} AU".format(ppar['rdisk'] / au))
      print(" Hrpivot = {:.2f} AU".format(ppar['hrpivot'] / au))
      print(" Hr(Hrpivot) = {:.2E}".format(ppar['hrdisk']))
      print(" Power law of H = {:.2f}".format(ppar['plh']))
      print(" Power law of surface density = {:.2f}".format(ppar['plsig1']))
      print(" Dust-to-gas = {:.2E}".format(ppar['dusttogas']))
   else:
      print(" *NO* disk is included!")

   if disk == True:
      # Calculate the pressure scale height as a function of r, phi
      hp = np.zeros([grid.nx, grid.ny, grid.nz], dtype=np.float64)

      hp[:,:,0] = ppar['hrdisk'] * (rcyl/ppar['hrpivot'])**ppar['plh'] * rcyl

      # Determine Sigma(r)
      sigma = np.zeros([grid.nx, grid.ny, grid.nz], dtype=np.float64)

      if ('rsig' in ppar.keys()):
          rsig = ppar['rsig']
      else:
          rsig = ppar['rdisk']
      if ('sig0' in ppar.keys()):
          sig0 = ppar['sig0']
      else:
          sig0 = 1.0  # we normalise it later to the requested mass
      dum1 = sig0 * (rcyl/rsig)**ppar['plsig1']

      # Broken power law outside of rdisk
      crit1 = ( rcyl > ppar['rdisk'] )
      sig_rdisk = sig0 * (ppar['rdisk']/rsig)**ppar['plsig1']
      if ppar['plsig2'] > -20.0:
         dum1[crit1] = sig_rdisk * (rcyl[crit1]/ppar['rdisk'])**ppar['plsig2']
      else:
         dum1[crit1] = 0.0
      # Within Rin
      crit2 = ( rcyl < ppar['rin'] )
      dum1[crit2] = 0.0
      
      # Adding the smoothed inner rim
      if ('srim_rout' in ppar.keys()) and ('srim_plsig' in ppar.keys()):
         sig_srim = 1.0 * (ppar['srim_rout']*ppar['rin'] / rsig)**ppar['plsig1']
         dum2 = sig_srim * (rcyl / (ppar['srim_rout']*ppar['rin']))**ppar['srim_plsig']
         p = -5.0
         dum = (dum1**p + dum2**p)**(1./p)
      else:
         dum = dum1
      sigma[:,:,0] = dum

      # Calculate rho_disk_tot(r,theta) density
      dum = sigma[:,:,0] / (hp[:,:,0] * np.sqrt(2.0*np.pi)) *    \
            np.exp(-0.5 * zz[:,:]**2 / hp[:,:,0]**2)
      # Copy to all z coordinates
      for iz in range(grid.nz):
         rho_disk_tot[:,:,iz,0] = dum

      # Calculate the volume of each grid cell
      vol  = grid.getCellVolume()
 
      # Now we calculate the mass in rho_disk_tot and scale the density to get back the
      # desired disk mass (['mdisk'] parameter) *only if ['mdisk'] is set*:
      if ('mdisk' in ppar.keys()):
         mass = (rho_disk_tot[:,:,:,0]*vol).sum(0).sum(0).sum(0)
         rho_disk_tot = rho_disk_tot * (ppar['mdisk']*0.5/mass) 
         # Note that: * 0.5 -> we only consider the upper half of the disk
      # Recompute the disk mass for consistency check
      mass = (2.0*rho_disk_tot[:,:,:,0]*vol).sum(0).sum(0).sum(0)
      
      # Determine/check Sigma(r) 
      dens = rho_disk_tot[:,:,0,0]
      vol = vol[:,:,0]
      surf_mass = np.zeros(grid.nx)
      surf_area = np.zeros(grid.nx)
      for ix in range(grid.nx):
        lind = rcyl >= grid.xi[ix]
        uind = rcyl < grid.xi[ix+1]
        ind = np.where(lind == uind)
        surf_mass[ix] = np.sum(2.*dens[ind] * vol[ind])
        surf_area[ix] = np.pi * (grid.xi[ix+1]**2 - grid.xi[ix]**2)
      sig_rsig = np.interp(rsig,grid.x,surf_mass/surf_area)
      
      # Print the final mass and the sig0
      print(" Mdisk (final, gas+dust) = {:.2E} Msol".format(mass/ms))
      print(" Sig0(rsig) (final, gas+dust) = {:.2E} g/cm^2".format(sig_rsig))
                    
      # Convert the gas+dust mass to dust mass
      rho_disk_dust = np.array(rho_disk_tot) * ppar['dusttogas']


#============== Adding up the density contributions =================#

   if ppar['ngs'] == 2 and envelope and disk:
        rhodust = np.zeros([grid.nx, grid.ny, grid.nz, ppar['ngs']])
        rhodust[:,:,:,0] = rho_disk_dust[:,:,:,0]
        rhodust[:,:,:,1] = (rho_env + rho_bg)[:,:,:,0]
        data.rhodust = rhodust
        opac_files = ppar['dustkappa_ext'][0:2]
   else:
        data.rhodust = rho_env + rho_disk_dust + rho_bg
        opac_files = ppar['dustkappa_ext'][0]

# --------------------------------------------------------------------------------------------
# Now write out everything 
# --------------------------------------------------------------------------------------------
   print("\nWriting the input files:\n")
   #Frequency grid
   grid.writeWavelengthGrid(old=False)
   #Spatial grid
   grid.writeSpatialGrid(old=False)
   #Input radiation field
   radSources.writeStarsinp(ppar=ppar, old=False)
   # Write the external radiation field input file if needed
   if isrf:
      ISradField(G,grid=grid,ppar=ppar,show=shisrf,write=isrf)
   #Dust density distribution
   data.writeDustDens(binary=False, old=False)
   #radmc3d.inp
   radmc3dPy.setup.writeRadmc3dInp(modpar=modpar)
   #Master dust opacity file
   opac=analyze.radmc3dDustOpac()
   opac.writeMasterOpac(ext=opac_files, scattering_mode_max=ppar['scattering_mode_max'], old=False)

def readParams(paramfile=None):
    '''
    Set default parameter values and/or read user defined parameter from file.
    '''
    
    # Read the parameters from the problem_params.inp file 
    modpar = analyze.radmc3dPar()
    
    if paramfile:
        modpar.readPar(fname=paramfile)
    else:
        # Set radmc3dPy default
        modpar.loadDefaults()
        # Set SimpleDiskEnv defaults

        # Radiation sources
        modpar.setPar(['mstar', '[3.0*ms]', 
                       ' Mass of the star(s)', 
                       'Radiation sources'])
        modpar.setPar(['rstar','[5.9*rs]', 
                       ' Radius of the star(s)', 
                       'Radiation sources'])
        modpar.setPar(['tstar','[4786.0]', 
                       ' Effective temperature of the star(s) [K]', 
                       'Radiation sources'])
        modpar.setPar(['staremis_type','["blackbody"]', 
                       ' Stellar emission type ("blackbody", "kurucz", "nextgen")', 
                       'Radiation sources'])

        # Grid parameters
        modpar.setPar(['crd_sys', "'sph'", 
                       ' Coordinate system used (car/cyl)', 
                       'Grid parameters']) 
        modpar.setPar(['nx', '[30,100]', 
                       ' Number of grid points in the first dimension (to switch off this dimension set it to 0)', 
                       'Grid parameters']) 
        modpar.setPar(['ny', '80', 
                       ' Number of grid points in the second dimension (to switch off this dimension set it to 0)', 
                       'Grid parameters'])
        modpar.setPar(['nz', '0', 
                       ' Number of grid points in the third dimension (to switch off this dimension set it to 0)', 
                       'Grid parameters'])
        modpar.setPar(['xbound', '[1.0*au, 1.05*au, 100.*au]', 
                       ' Boundaries for the x grid', 'Grid parameters'])
        modpar.setPar(['ybound', '[0.0, pi/2.]', 
                       ' Boundaries for the y grid', 
                       'Grid parameters'])
        modpar.setPar(['zbound', '[0.0, 2.0*pi]', 
                       ' Boundraries for the z grid', 
                       'Grid parameters'])

        # Dust opacity
        modpar.setPar(['lnk_fname', "['./lnk/mix_compact_bruggeman.lnk']", 
                       ' ', 'Dust opacity'])
        modpar.setPar(['gdens', '[1.8]', 
                       ' Bulk density of the materials in g/cm^3', 'Dust opacity'])
        modpar.setPar(['gsmin', '0.1', ' Minimum grain size', 'Dust opacity'])
        modpar.setPar(['gsmax', '10.0', ' Maximum grain size', 'Dust opacity'])
        modpar.setPar(['ngs', '10', ' Number of grain sizes', 'Dust opacity'])
        modpar.setPar(['gsdist_powex', '-3.5', 
                       ' Grain size distribution power exponent', 'Dust opacity'])
        modpar.setPar(['mixabun', '[1.0]', 
                       ' Mass fractions of the dust components to be mixed', 'Dust opacity'])
        modpar.setPar(['dustkappa_ext',"['silicate']", ' ', 'Dust opacity'])

        # Code parameters
        modpar.setPar(['scattering_mode_max', '0', 
                       ' 0 - no scattering, 1 - isotropic scattering, 2 - anizotropic scattering', 'Code parameters'])
        modpar.setPar(['istar_sphere', '1', 
                       ' 1 - take into account the finite size of the star, 0 - take the star to be point-like', 'Code parameters'])
        modpar.setPar(['itempdecoup', '1', 
                       ' Enable for different dust components to have different temperatures', 'Code parameters'])
        modpar.setPar(['tgas_eq_tdust', '1', 
                       ' Take the dust temperature to identical to the gas temperature', 'Code parameters'])
        modpar.setPar(['modified_random_walk', '1', 
                       ' Switched on (1) and off (0) modified random walk', 'Code parameters'])
        
        # Envelope parameters
        modpar.setPar(['bgdens', '0.0e0', ' Background density (g/cm^3)', 
                       'Envelope parameters'])
        modpar.setPar(['dusttogas', '1.0e-2', ' Dust-to-gas mass ratio', 
                       'Envelope parameters'])
        modpar.setPar(['modeEnv', "'Ulrich1976'", 
                       " Choose envelope model, options: ['Ulrich1976','Tafalla2004','powerlaw']", 
                       'Envelope parameters'])
        modpar.setPar(['rho0Env', '4.e-20', 
                       ' New central density g/cm^3 dust density volume', 
                       'Envelope parameters'])
        modpar.setPar(['r0Env', '300.0*au', 
                       " Flattening radius in 'Tafalla2004' or centrifugal radius in 'Ulrich1976' models", 'Envelope parameters'])
        modpar.setPar(['rTrunEnv', '30.0*au', ' Truncation radius', 
                       'Envelope parameters'])
        modpar.setPar(['redFactEnv', '1.0e-2', 
                       ' Density is reduced by this factor if r < rTrunEnv', 
                       'Envelope parameters'])
        
        # Disk parameters
        modpar.setPar(['mdisk', '0.01*ms', ' Disk mass', 
                       'Disk parameters'])
        modpar.setPar(['rin', '1.0*au', ' Inner disk radius', 
                       'Disk parameters'])
        modpar.setPar(['rdisk', '50.0*au', ' Outer disk radius', 
                       'Disk parameters'])
        modpar.setPar(['hrdisk', '0.1', 
                       ' Ratio of the pressure scale height over radius at hrpivot', 
                       'Disk parameters'])
        modpar.setPar(['hrpivot', '50.0*au', 
                       ' Reference radius at which Hp/R is taken',
                       'Disk parameters'])
        modpar.setPar(['plh', '2.0/7.0', ' Flaring index', 
                       'Disk parameters'])
        modpar.setPar(['plsig1', '-1.0', 
                       ' Power exponent of the surface density distribution as a function of radius', 
                       'Disk parameters'])
        modpar.setPar(['plsig2', '-40.0', 
                       ' Power law exponent at r > rdisk (abrubt cutoff at rdisk is not realistic)', 
                       'Disk parameters'])
        
    # Return
    return modpar
