{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Envelope irradiated by interstellar radiation filed\n",
    "\n",
    "In this example an Ulrich (1976) envelope model with varying radiation sources. The model can be heated by the internal stellar source and/or the Interstellar Radiation Field (ISRF) as described by Draine (1978) and Black (1994).\n",
    "\n",
    "The goal of this example is to illustrate the steps of model creation with the SimpleDiskEnv module and to test the effect including / neglecting the standard ISRF."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Import modules\n",
    "import os               # we use this for folder related commands\n",
    "import shutil           # we use this for file copying\n",
    "\n",
    "import radmc3dPy\n",
    "import SimpleDiskEnv\n",
    "\n",
    "# In line plotting\n",
    "%matplotlib inline\n",
    "\n",
    "# Function to create working directory and to copy opacity files\n",
    "def makedir(model,WORK_DIR):\n",
    "    if not os.path.isdir(WORK_DIR+'/'+model):\n",
    "        os.makedirs(WORK_DIR+'/'+model)\n",
    "    os.chdir(WORK_DIR+'/'+model)\n",
    "    # Copy required files\n",
    "    file_source = [WORK_DIR+'/problem_ulrich.inp',\n",
    "                   WORK_DIR+'/dustkappa_osshenn_thinextra.inp']\n",
    "    file_dest = WORK_DIR+'/'+model\n",
    "    for i in range(len(file_source)):\n",
    "        shutil.copy2(file_source[i],file_dest)\n",
    "        \n",
    "# Define root work directory\n",
    "WORK_DIR = os.getcwd()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 1) Ulrich (1974) flattened envelope model with stellar irradiation\n",
    "\n",
    "#### Create example working directory in the local folder"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "makedir('ulrich_noExt',WORK_DIR)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Write parameter file for a basic Ulrich (1976) type flattened envelope model. \n",
    "\n",
    "Note that disk and cavity parameters are not defined. If you call SimpleDiskEnv.simpleRadmc3Dmodel() with disk=True and/or cavity=True arguments, then the code will fail because parameters for these components are not defined.\n",
    "\n",
    "Note that the parameter file already exists in the examples directory. In the next sections we will use its copy.\n",
    "\n",
    "The parameter values are those found for Elisa29 in Miotello et al. (Astronomy & Astrophysics, Volume 567, A32)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%file problem_ulrich.inp\n",
    "#\n",
    "# Block: Radiation sources\n",
    "#\n",
    "mstar       = [2.5*ms]         # Mass of the star(s)\n",
    "pstar       = [0.0, 0.0, 0.0]  # Position of the star(s) (cartesian coordinates)\n",
    "rstar       = [5.9*rs]         # Radius of the star(s)\n",
    "tstar       = [4786.0]         # Effective temperature of the star(s) [K]\n",
    "#\n",
    "# Block: Grid parameters\n",
    "#\n",
    "crd_sys     = 'sph'            # Coordinate system used (car/cyl)\n",
    "xbound      = [1.*au,1.05*au,5e4*au] # Boundaries for the x grid\n",
    "nx          = [30,100]         # Number of grid points in the first dimension\n",
    "ybound      = [0.,pi/2.]       # Boundaries for the y grid\n",
    "ny          = 80               # Number of grid points in the second dimension\n",
    "zbound      = [0.,2.0*pi]      # Boundaries for the z grid\n",
    "nz          = 0                # Number of grid points in the third dimension\n",
    "#\n",
    "# Block: Wavelength grid parameters\n",
    "#\n",
    "nw          = [50,150,100]     # Number of points in the wavelength grid\n",
    "wbound      = [0.1,7.,25.,1e4] # Boundaries for the wavelength grid\n",
    "#\n",
    "# Block: Dust opacity\n",
    "#\n",
    "dustkappa_ext = ['osshenn_thinextra']  # Opacity file extension: dustkappa_[ext].inp\n",
    "ngs         = 1                # Number of grain sizes\n",
    "#\n",
    "# Block: Code parameters\n",
    "#\n",
    "istar_sphere         = 1         # 1 - take into account the finite size of the star, 0 - take the star to be point-like\n",
    "nphot                = long(1e6) # Nr of photons for the thermal Monte Carlo\n",
    "modified_random_walk = 1         # Use the modified random walk method to improve speed?\n",
    "scattering_mode_max  = 0         # 0 - no scattering, 1 - isotropic scattering, 2 - anisotropic scattering\n",
    "#\n",
    "# Block: Model parameters\n",
    "#\n",
    "bgdens      = 1e-40            # Background density (g/cm^3)\n",
    "dusttogas   = 0.01             # Dust-to-gas mass ratio\n",
    "modeEnv     = 'Ulrich1976'     # Choose envelope model, options: ['Ulrich1976','Tafalla2004','powerlaw']\n",
    "rho0Env     = 4e-20            # New central density gr/cm3 dust density volume\n",
    "r0Env       = 300.0*au         # Flattening radius in 'Tafalla2004' or centrifugal radius in 'Ulrich1976' models\n",
    "rTrunEnv    = 30.0*au          # Truncation radius\n",
    "redFactEnv  = 1e-2             # Density is reduced by this factor if r < rTrunEnv"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Create model and determine dust temperature\n",
    "\n",
    "In this step input files are written and Monte Carlo radiative transfer mode of RADMC-3D is called in order to compute the dust temparature."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "# Write input files for RADMC-3D\n",
    "SimpleDiskEnv.simpleRadmc3Dmodel(disk=False, cavity=False, \n",
    "                                 paramfile='problem_ulrich.inp')\n",
    "\n",
    "# Run the model using multi-threaded mode (4 cores)\n",
    "print (\"\\nRADMC-3D runtime:\")\n",
    "!time radmc3d mctherm setthreads 4 > radmc3d.log"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Density and temperature\n",
    "\n",
    "Let's plot the density and temperature distribution."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Read and plot the model density and temperature\n",
    "ulrich_noExt = radmc3dPy.analyze.readData(dtemp=True,ddens=True,\n",
    "                                          binary=False)\n",
    "\n",
    "# Plot model\n",
    "SimpleDiskEnv.plotModel(ulrich_noExt,xlog=False,\n",
    "                        rlim=[0,5e4],zlim=[0,5e4],\n",
    "                        rhomin=-24,rhomax=-18,\n",
    "                        Tmax=50.)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 2) Ulrich envelope irradiated only by ISRF\n",
    "\n",
    "Let's use only the ISRF as radiation source and run the model in one step."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "# Create an envelope model without external radiation:\n",
    "makedir('ulrich_noStar',WORK_DIR)\n",
    "\n",
    "SimpleDiskEnv.simpleRadmc3Dmodel(disk=False,cavity=False,\n",
    "                              isrf=True,                         # Set up ISRF\n",
    "                              shisrf=True,                       # Show the ISRF\n",
    "                              paramfile='problem_ulrich.inp')\n",
    "\n",
    "# Remove stellar irradiation input file in order to switch it off\n",
    "!rm stars.inp\n",
    "\n",
    "# Run the model using multi-threaded mode (4 cores)\n",
    "print (\"\\nRADMC-3D runtime:\")\n",
    "!time radmc3d mctherm setthreads 4 > radmc3d.log\n",
    "\n",
    "# Read and plot the model density and temperature\n",
    "print ('\\n')\n",
    "ulrich_noStar = radmc3dPy.analyze.readData(dtemp=True,ddens=True,\n",
    "                                        binary=False)\n",
    "SimpleDiskEnv.plotModel(ulrich_noStar,xlog=False,\n",
    "                        rlim=[0,5e4],zlim=[0,5e4],\n",
    "                        rhomin=-24,rhomax=-18,\n",
    "                        Tmax=50.)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 3) Ulrich model with stellar and ISRF heating"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "# Create an envelope model without external radiation:\n",
    "makedir('ulrich_ExtStar',WORK_DIR)\n",
    "    \n",
    "SimpleDiskEnv.simpleRadmc3Dmodel(disk=False,cavity=False,\n",
    "                              isrf=True,shisrf=False,\n",
    "                              paramfile='problem_ulrich.inp')\n",
    "\n",
    "# Run the model using multi-threaded mode (4 cores)\n",
    "print (\"\\nRADMC-3D runtime:\")\n",
    "!time radmc3d mctherm setthreads 4 > radmc3d.log\n",
    "\n",
    "# Read and plot the model density and temperature\n",
    "print ('\\n')\n",
    "ulrich_ExtStar = radmc3dPy.analyze.readData(dtemp=True,ddens=True,\n",
    "                                        binary=False)\n",
    "SimpleDiskEnv.plotModel(ulrich_ExtStar,xlog=False,\n",
    "                        rlim=[0,5e4],zlim=[0,5e4],\n",
    "                        rhomin=-24,rhomax=-18,\n",
    "                        Tmax=50.)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
